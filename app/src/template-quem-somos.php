<?php get_header(); 

/* Template Name: Quem Somos */

if (have_posts()) : 
    while (have_posts()) : the_post(); 

     $title = get_the_title();
     $missao = get_field('missao');
     $visao = get_field('visao');
     $valores = get_field('valores');
     $parceiros = get_field('parceiros');
     $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

    endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="quem-somos">
	<div class="container sidebar">
    <article class="content-full">
		            <picture>
				<img src="<?php echo $img[0]; ?>" alt="<?php echo $title; ?>">
            </picture>

				<h2>Quem Somos</h2>
			<?php the_content( ); ?>
			<hr>
				<h2>Parceiros</h2>
			<div class="parceiros">
				<?php foreach($parceiros as $parceiro): ?>
					<div class="box-parceiros">
						 <picture>
							<img src="<?php echo $parceiro['logo']; ?>" alt="<?php echo $parceiro['titulo']; ?>">
						</picture>
						<h3><?php echo $parceiro['especialidade']; ?></h3>
						<p><?php echo $parceiro['resumo']; ?></p>

					</div>
				<?php endforeach; ?>
			</div>
			
        </article>
		<aside class="others-list">
			<div class="box-quem-somos">
				<div class="box-content">					
					<strong>
						Missão
					</strong>
                        <?php echo $missao; ?>
				</div>
			</div>
			<div class="box-quem-somos">
				<div class="box-content">					
					<strong>
						Visão
					</strong>
                        <?php echo $visao; ?>
				</div>
			</div>
			<div class="box-quem-somos">
				<div class="box-content">					
					<strong>
						Valores
					</strong>
                        <?php echo $valores; ?>
				</div>
			</div>

        </aside>
	</div>
</section>
<?php get_footer(); ?>