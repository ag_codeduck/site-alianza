$(document).ready(function() {
	featuredMobile();
	bannerHome();
	modal();
	passaPDF();
	validaForm();

	function featuredMobile() {
		var featuredList = $('.featured-list');

		$(featuredList).slick({
			prevArrow: $('.prev-featured'),
			nextArrow: $('.next-featured'),
			responsive: [
			{
				breakpoint:2560,
				settings: "unslick",

			},
			{
				breakpoint: 768,
				dots: false,
				infinite: false,
				speed: 300,
				slidesToShow: 1,
			}
			]
		});
	}

	function bannerHome(){
		$('.banner .container').slick({
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			fade: true,
			autoplay: true,
			prevArrow: $('.prev'),
			nextArrow: $('.next'),
		});
	}

	function modal(modalTeste){		
		$("#modal").iziModal({
			transitionIn:'fadeInDown',
			transitionOut: 'fadeOutDown'
		});
		$(document).on('click', '.trigger', function (event) {
			event.preventDefault();
			$('#modal').iziModal('open');
		});
	}

	function validaForm(){

		var telMask = ['(99) 99999-9999', '(99) 9999-9999'];
		var tel = document.querySelector('#phone');
		VMasker(tel).maskPattern(telMask[0]);

		var celMask = ['(99) 99999-9999', '(99) 9999-9999'];
		var cel = document.querySelector('#mobile');
		VMasker(cel).maskPattern(celMask[0]);

		// var docMask = ['99.999.999/9999-99'];
		// var doc = document.querySelector('#cnpj');
		// if(doc){
		// 	VMasker(doc).maskPattern(docMask[0]);
		// }
	}

	function passaPDF(){
		$(".botoes .trigger").click(function(){
			var linkPdf = $(this).attr("data-pdf");
			var downloadPdf = $('#downloadPDF');
			downloadPdf.attr("href", linkPdf);
		});

	}

});